# Javali Error Pages

This module adds a custom 403/404/maintenance page.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/javali_error_pages).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/javali_error_pages).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no modifiable settings. There is no configuration.


## Maintainers

- João Mauricio - [jmauricio](https://www.drupal.org/u/jmauricio)
- Mário Martins - [mario.martins](https://www.drupal.org/u/mariomartins)
- Daniela Costa - [daniela.costa](https://www.drupal.org/u/danielacosta)
- Paulo Calado - [kallado](https://www.drupal.org/u/kallado)
- Ricardo Salvado - [rsalvado](https://www.drupal.org/u/rsalvado)
